#include "Application.h"
#include "LoginScreen.h"


Application::Application() : Game()
{
}


Application::~Application()
{
}

int main(){
	Application app;
	app.setScreen(new LoginScreen());
	app.startGame();
	return 0;
}
