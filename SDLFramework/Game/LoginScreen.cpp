#include "LoginScreen.h"

#include <iostream>

LoginScreen::LoginScreen() : tex("Assets/Login/background.png",0,0), button("testbutton","Assets/Login/dankbutton.png", 150, 150)
, buttonS("testbutton", "Assets/Login/dankbutton.png", 150, 250)
{
	name = "blayt";	
}


LoginScreen::~LoginScreen()
{

}

void LoginScreen::update(){
	if(button.clicked()){
		std::cout << "Baka" << std::endl;
	}
	if(buttonS.clicked()){
		std::cout << "Bakaa" << std::endl;
	}
}

void LoginScreen::render(){
	tex.render();
	button.render();
	buttonS.render();
}

void LoginScreen::handleEvent(SDL_Event* event){
	button.handleEvent(event);
	buttonS.handleEvent(event);
}