#pragma once

#include "SDLFramework.h"

class LoginScreen : public Screen
{
private:
	Texture tex;
	Button button;
	Button buttonS;
public:
	LoginScreen();
	~LoginScreen();

	virtual void update();
	virtual void render();
	virtual void handleEvent(SDL_Event * event);
};

