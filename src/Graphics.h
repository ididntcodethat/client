#ifndef GRAPHICS_H
#define GRAPHICS_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>

#include <iostream>
#include <vector>

class Texture;

class Graphics
{
    public:
        Graphics();

        SDL_Renderer* getRenderer();
        SDL_Window* getWindow();
        SDL_Event& getEvent();

        void removeGraphics();

    private:
        bool initializeSDL();

        SDL_Renderer* graphicsRenderer;
        SDL_Window* graphicsWindow;
        SDL_Event graphicsEvent;
};

class Texture
{
    private:
        //Vector that storage every clip.
        std::vector<SDL_Rect> tile;

        //Most important variables.Do not forget, they're important!
        SDL_Renderer* engineRenderer;
        SDL_Texture* texture;

        //Rect for our texture
        SDL_Rect textureRect;

        //Less important variables but still important.
        int width,height,x,y;

        void loadTexture(std::string);

    public:
        //If don't need clips for your texture you can use this constructor to avoid creating clips.
        Texture(std::string, SDL_Renderer*, bool, int, int);

        virtual ~Texture();

        //So this method return a id of clip but if that clip doesn't exist it always gonna return 0;
        const SDL_Rect* getTile(int id);

        const SDL_Rect* getTextureRect();

        SDL_Texture* getTiledTexture();

        //Here we create a clips for every texture in our tileset.
        void prepareClips(int = 32, int = 32);

        void removeTexture();

        void loadTTF(std::string);

        void render();

        void render(int);

        int getWidth();

        int getHeight();

        void setHeight(int);

        void setWidth(int);

        void setX(int);

        void setY(int);

};

#endif GRAPHICS_H
