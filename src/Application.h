#ifndef APPLICATION_H
#define APPLICATION_H

#include "Graphics.h"
#include "GameStates.h"
#include "StateControler.h"

class Application
{
    public:
        Application() : applicationQuit(false), graphics(new Graphics()), loginMenu(new LoginScreen(graphics -> getRenderer())), characterMenu(new CharacterSelect())
        {
            stateControler = new StateControler(loginMenu, characterMenu);
        }
        
        void applicationLoop();

        void applicationClose();

    private:
        Graphics* graphics;
        
        StateControler* stateControler;
        
        LoginScreen* loginMenu;
        CharacterSelect* characterMenu;

        bool applicationQuit;

        void render();


};

#endif APPLICATION_h