#ifndef INTERFACECOMPONENTS_H
#define INTERFACECOMPONENTS_H

#include <SDL2/SDL.h>

#include <vector>
#include <iostream>

#include "Graphics.h"

class TextInput;
class Button;
class Window;
class Screen;

class Component
{
	protected:
		//Do I have to explain this. #topkek
		std::string componentName;

		//Path to component texture.
		std::string texturePath;

		//Size of our component.
		int componentHeight;
		int componentWidth;

		//Position of our component.
		int componentX;
		int componentY;

		//If component was clicked this variable will be changed to true.
		bool isComponentActive;

		//Texture of our component
		Texture* componentTexture;

		SDL_Renderer* graphicsRenderer;

	public:

		Component(std::string name, std::string path, int x, int y, SDL_Renderer* graphicsRenderer, int width, int height);

		bool isActive();

		void activateComponent();

		void eventHandler(SDL_Event*);

		//This method returns true if object was clicked by mouse.
		bool onClick(int, int);

		void keyboardEvent();

		void mouseEvent();

		void componentRender();

};

class Screen : public Component
{
	private:
		std::vector<Window*> windows;
	public:
		using Component::Component;

		void createWindow(std::string, std::string, int, int, int, int);

		void createWindow(std::string, bool, int, int, int, int);

		void changeWindowPosition(int, int);

		void eventHandler(SDL_Event*);
};

class Window : public Component
{
	private:

		std::vector<Button*> buttons;
		std::vector<TextInput*> textInputs;

	public:
		using Component::Component;

		void createButton();

		void createTextInput();

		void getButton();

		void getTextInput();

};

class TextInput : public Component
{
	private:
		std::string currentText;
	public:
		using Component::Component;

		void keyboardEvent();

		void clearTextInput();

};

class Button : public Component
{
	private:

	public:
		using Component::Component;
};

#endif
