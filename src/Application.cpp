#include "Application.h"
#include "Graphics.h"
#include "SDL2/SDL.h"


void Application::applicationLoop()
{
    while(!applicationQuit)
    {
        while(SDL_PollEvent(&graphics -> getEvent()) != 0)
        {
            if(graphics -> getEvent().type == SDL_QUIT)
                applicationQuit = true;

            stateControler -> sendEvent(&graphics -> getEvent());
        }
        render();
    }
}

void Application::applicationClose()
{

	delete stateControler;

	loginMenu -> remove();
	delete loginMenu;
	loginMenu = NULL;

	delete characterMenu;

	graphics -> removeGraphics();
	delete graphics;
	graphics = NULL;
}

void Application::render()
{
	SDL_RenderClear(graphics -> getRenderer());

	stateControler -> renderCurrentState();

	SDL_RenderPresent(graphics -> getRenderer());
}
