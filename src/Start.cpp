#include "Application.h"
#include <iostream>

Application app;

int main()
{
    app.applicationLoop();
    app.applicationClose();
}