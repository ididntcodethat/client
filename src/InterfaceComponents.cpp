#include "InterfaceComponents.h"

#include <iostream>

#include <SDL2/SDL.h>

/*
      Component
*/


/*
  Its very intial version of this constructor.
*/

Component::Component(std::string name, std::string path, int x, int y, SDL_Renderer* graphicsRenderer, int width, int height) : isComponentActive(false),
 componentName(name), texturePath(path), componentX(x), componentY(y), componentWidth(width), componentHeight(height), graphicsRenderer(graphicsRenderer)
{
  if(componentName.empty())
    componentName = "Empty name.";

  if(componentX == NULL)
    componentX = 0;
  if(componentY == NULL)
    componentY = 0;

  if(!texturePath.empty())
    if(graphicsRenderer != NULL)
      componentTexture = new Texture(texturePath, graphicsRenderer, false, componentX, componentY);
    else
      std::cout << "Can't create component when renderer is equal to null." << std::endl;

  if(componentTexture != NULL)
  {
  if(componentWidth == NULL)
    componentWidth = componentTexture -> getWidth();
  if(componentHeight == NULL)
    componentHeight = componentTexture -> getHeight();
  }
  else
  {
    if(componentWidth == NULL)
      componentWidth = 0;
    if(componentHeight == NULL)
      componentHeight = 0;
  }

}



bool Component::isActive()
{
  return isComponentActive;
}

bool Component::onClick(int clickX, int clickY)
{
  if(clickX >= componentX && clickX < componentX + componentWidth && clickY >= componentY && clickY < componentY + componentHeight)
    return true;
  else
    return false;
}

void Component::eventHandler(SDL_Event* componentEvent)
{
  int x,y;

  if(componentEvent -> type == SDL_MOUSEBUTTONDOWN)
  {
    SDL_GetMouseState(&x, &y);

    if(onClick(x, y))
      mouseEvent();
    else
      std::cout << "Click was on outside of the object." << std::endl;
  }

}

void Component::mouseEvent()
{
  std::cout << "Component was clicked" << std::endl;
}

void Component::componentRender()
{
  componentTexture -> render();
}

/*
      Screen
*/

//Screen management

void Screen::createWindow(std::string windowName, std::string windowTexturePath, int x, int y, int width, int height)
{

}

void Screen::createWindow(std::string windowName, bool createTexture, int x, int y, int width, int height)
{
  if(!createTexture)
  {

  }
}

void Screen::changeWindowPosition(int x, int y)
{

}

//Event related

void Screen::eventHandler(SDL_Event* screenEvent)
{
  int x,y;

  if(screenEvent -> type == SDL_MOUSEBUTTONDOWN)
  {
    SDL_GetMouseState(&x, &y);
  }

}
