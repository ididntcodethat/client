#include "GameStates.h"
#include "Graphics.h"

#include <iostream>
#include <vector>

#include <SDL2/SDL.h>

/*
                LoginScreen
*/

LoginScreen::LoginScreen(SDL_Renderer* graphicsRenderer) : graphicsRenderer(graphicsRenderer), button(new Button("Test button", "Assets/Login/dankbutton.png", 300, 300, graphicsRenderer, NULL, NULL))
{
    loadBackground();
}

void LoginScreen::render()
{
	for(int x = 0; x < textures.size(); x++)
		SDL_RenderCopy(graphicsRenderer, textures[x] -> getTiledTexture(), NULL, textures[x] -> getTextureRect());
}

void LoginScreen::receiveEvent(SDL_Event* event)
{

}

void LoginScreen::loadBackground()
{
	textures.push_back(new Texture("Assets/Login/background.png", graphicsRenderer, false, 0, 0));
}

void LoginScreen::initialize()
{
    std::cout << "Login screen initialized" << std::endl;
}

void LoginScreen::remove()
{
    std::cout << "Login screen removed" << std::endl;
    	for(int x = 0; x < textures.size(); x++)
    	{
    		textures[x] -> removeTexture();
    		delete textures[x];
    	}
}

/*
				Character Selection
*/
