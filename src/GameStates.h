#ifndef GAMESTATES_H
#define GAMESTATES_H

#include <SDL2/SDL.h>

#include "InterfaceComponents.h"

#include <vector>

class CharacterSelect;
class Texture;

class LoginScreen
{
    public:
        LoginScreen(SDL_Renderer*);

        void initialize();

        void receiveEvent(SDL_Event*);

        void remove();

        void render();

        void update();

    private:
    	static bool alreadyInitialized;

    	SDL_Renderer* graphicsRenderer;

      //For testing purposes
      Button* button;

    	std::vector<Texture*> textures;

		void loadBackground();
};

class CharacterSelect
{

};

#endif
