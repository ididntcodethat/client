#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>

#include "Graphics.h"

#include <iostream>

/*
                Graphics
*/

Graphics::Graphics()
{
    if(initializeSDL())
        std::cout << "Error in loading of graphics module." << std::endl;
}

bool Graphics::initializeSDL()
{
    bool somethingFailed = false;


    if(SDL_Init(SDL_INIT_EVERYTHING) < 0)
    {
         std::cout << "Can't initialize SDL" << std::endl;
         somethingFailed = true;
    }
    else
        graphicsWindow = SDL_CreateWindow("Tales of Maciuszek", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1024, 768, SDL_WINDOW_SHOWN);
        if(graphicsWindow == NULL)
        {
            std::cout << "Can't create window" << std::endl;
            somethingFailed = true;
        }
        else
            graphicsRenderer = SDL_CreateRenderer(graphicsWindow, -1, SDL_RENDERER_ACCELERATED);
            if(graphicsRenderer == NULL)
            {
                std::cout << "Can't create renderer" << std::endl;
                somethingFailed = true;
            }
            else
            {
                SDL_SetRenderDrawColor(graphicsRenderer, 0xFF, 0xFF, 0xFF, 0xFF);

                int imgFlags = IMG_INIT_PNG;
                if(!(IMG_Init(imgFlags)) & imgFlags)
                {
                    std::cout << "Can't initialize IMG" << std::endl;
                    somethingFailed = true;
                }
                else if(TTF_Init() == -1)
                {
                    std::cout << "Can't initialize TTF" << std::endl;
                    somethingFailed = true;
                }
            }

    return somethingFailed;
}

void Graphics::removeGraphics()
{
    SDL_DestroyRenderer(graphicsRenderer);
    SDL_DestroyWindow(graphicsWindow);
    graphicsRenderer = NULL;
    graphicsWindow = NULL;

    IMG_Quit();
    SDL_Quit();
}

/*
    Getters
*/

SDL_Renderer* Graphics::getRenderer()
{
    return graphicsRenderer;
}

SDL_Event& Graphics::getEvent()
{
    return graphicsEvent;
}


/*


                Texture


*/

Texture::Texture(std::string path, SDL_Renderer* engineRenderer, bool createClips, int x, int y) : engineRenderer(engineRenderer), x(x), y(y)
{
    if(createClips == false)
        loadTexture(path);
    else
    {
        loadTexture(path);
        prepareClips();
    }
}

Texture::~Texture()
{

}

/*
        Texture management
*/

void Texture::loadTexture(std::string path)
{
    SDL_Surface* image = IMG_Load(path.c_str());
    if(image == NULL)
    {
        printf("Unable to load texture");
    }
    else
    {
        texture = SDL_CreateTextureFromSurface(engineRenderer,image);

        if(texture == NULL)
            std::cout << SDL_GetError() << std::endl;

        //Here we pass width and height of our texture to created before veriables (int width, height) in Texture.h
        SDL_QueryTexture(texture, NULL, NULL, &width, &height);

        textureRect.w = width;
        textureRect.h = height;
        textureRect.x = x;
        textureRect.y = y;

        SDL_FreeSurface(image);
    }
}

void Texture::prepareClips(int rowSize, int columnSize)
{
    for(int row = 0; row < (height / rowSize); row++)
    {
        for(int column = 0; column < (width / columnSize); column++)
        {
            SDL_Rect clip;

            clip.x = column * 32;
            clip.y = row * 32;
            clip.w = 32;
            clip.h = 32;

            tile.push_back(clip);
        }
    }
    //std::cout << "Prepared:" << tile.size() <<"(" << tile.size() - 1 << ")" << "clips." << std::endl;
}


void Texture::removeTexture()
{
    SDL_DestroyTexture(texture);
    texture = NULL;
}

/*
    Render related
*/

void Texture::render()
{
  SDL_RenderCopy(engineRenderer, getTiledTexture(), NULL, getTextureRect());
}

void Texture::render(int id)
{
  SDL_RenderCopy(engineRenderer, getTiledTexture(), getTile(id), getTextureRect());
}

/*
    Getters
*/

int Texture::getWidth()
{
    return width;
}

int Texture::getHeight()
{
    return height;
}

const SDL_Rect* Texture::getTextureRect()
{
    return &textureRect;
}

SDL_Texture* Texture::getTiledTexture()
{
    return texture;
}

const SDL_Rect* Texture::getTile(int id)
{
    if(id < tile.size())
        return &tile[id];
    else
        return &tile[0];
}

/*
    Setters
*/

void Texture::setHeight(int height)
{
    textureRect.h = height;
    this -> height = height;
}

void Texture::setWidth(int width)
{
    textureRect.w = width;
    this -> width = width;
}

void Texture::setX(int x)
{
    textureRect.x = x;
}

void Texture::setY(int y)
{
    textureRect.y = y;
}
