#ifndef STATECONTROLER_H
#define STATECONTROLER_H

#include "GameStates.h"

#include <SDL2/SDL.h>

#include <iostream>

/*
    Default state is loginScreen;
*/

class StateControler
{
    public:
        StateControler(LoginScreen *loginMenu,CharacterSelect *characterMenu) : loginMenu(loginMenu), characterMenu(characterMenu), currentState(nothing)
        {
            changeState(loginScreen);
        }
        
        enum States
        {nothing,loginScreen,characterSelection,inGame};
    
        int changeState(States);

        void sendEvent(SDL_Event*);
        
        void removeCurrentState();
        
        void renderCurrentState();
        
        void updateCurrentState();
        
        //Pointers to objects that represents each state.
        LoginScreen* loginMenu;
        CharacterSelect* characterMenu;   
             
    private:
        States currentState; 
};

#endif

