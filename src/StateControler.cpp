#include "StateControler.h"

#include <iostream>

#include <SDL2/SDL.h>

/*
                StateControler
*/

int StateControler::changeState(States state)
{
    if(state == loginScreen)
    {
        if(currentState == nothing)
        {
            currentState = loginScreen;
            loginMenu -> initialize();
        }
        else
        {
            removeCurrentState();
        }
    }

    if(state == nothing)
    {
        currentState = nothing;
    }
}

void StateControler::sendEvent(SDL_Event* event)
{
    if(currentState == loginScreen)
        loginMenu -> receiveEvent(event);
}

void StateControler::removeCurrentState()
{
    if(currentState == loginScreen)
    {
        loginMenu -> remove();
        std::cout << "loginMenu was removed from currentState" << std::endl;
    }

    if(currentState == nothing)
        std::cout << "Nothing was removed from current state" << std::endl;

}

void StateControler::renderCurrentState()
{
    if(currentState == loginScreen)
    {
        loginMenu -> render();
    }

}

void StateControler::updateCurrentState()
{

}